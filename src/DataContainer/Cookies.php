<?php

namespace ppag\TrackingManagerBundle\DataContainer;

use Contao\Backend;
use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\DataContainer;
use Contao\Image;
use Contao\Input;
use Contao\StringUtil;
use ppag\TrackingManagerBundle\Model\Cookie;
use Symfony\Component\VarDumper\VarDumper;

class Cookies extends Backend
{

    public function onloadCallback(DataContainer $dc)
    {
        $cookie = Cookie::findByPk($dc->id);

        if(Null === $cookie){
            return;
        }

        // Add template selection to all cookies, except the base cookie.
        if (!$cookie->isBaseCookie) {
            PaletteManipulator::create()
                ->addLegend('template_legend', 'title_legend', PaletteManipulator::POSITION_AFTER)
                ->addField('templates', 'template_legend', PaletteManipulator::POSITION_APPEND)
                ->applyToPalette('default', Cookie::getTable());
        }
    }

    /**
     * @return array
     */
    public function getTemplates()
    {

        $arrAllTemplates = array();

        /** @var SplFileInfo[] $files */
        $files = \Contao\System::getContainer()->get('contao.resource_finder')->findIn('templates')->files();

        foreach ($files as $file)
        {
            $strRelpath = StringUtil::stripRootDir($file->getPathname());
            $strModule = preg_replace('@^(vendor/([^/]+/[^/]+)/|system/modules/([^/]+)/).*$@', '$2$3', strtr($strRelpath, '\\', '/'));
            $arrAllTemplates[$strModule][str_replace('.html5','',basename($strRelpath))] = basename($strRelpath);
        }

        $strError = '';

        // Copy an existing template
        if (Input::post('FORM_SUBMIT') == 'tl_create_template')
        {
            $strOriginal = Input::post('original', true);

            if (\Contao\Validator::isInsecurePath($strOriginal))
            {
                throw new RuntimeException('Invalid path ' . $strOriginal);
            }

            $strTarget = Input::post('target', true);

            if (\Contao\Validator::isInsecurePath($strTarget))
            {
                throw new RuntimeException('Invalid path ' . $strTarget);
            }

            $projectDir = \Contao\System::getContainer()->getParameter('kernel.project_dir');

            // Validate the target path
            if (strncmp($strTarget, 'templates', 9) !== 0 || !is_dir($projectDir . '/' . $strTarget))
            {
                $strError = sprintf($GLOBALS['TL_LANG']['tl_templates']['invalid'], $strTarget);
            }
            else
            {
                $blnFound = false;
            }
        }

        return $arrAllTemplates;
    }

    /**
     * Return the "toggle visibility" button
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (Input::get('tid')) {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1),
                (@func_get_arg(12) ?: null));
            $this->redirect($this->getReferer());
        }


        $href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['published'] ? '' : 1);

        if (!$row['published']) {
            $icon = 'invisible.svg';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon,
                $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }


    /**
     * Disable/enable a user group
     *
     * @param integer              $intId
     * @param boolean              $blnVisible
     * @param Contao\DataContainer $dc
     *
     * @throws Contao\CoreBundle\Exception\AccessDeniedException
     */
    public function toggleVisibility($intId, $blnVisible, Contao\DataContainer $dc = null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc) {
            $dc->id = $intId; // see #8043
        }

        // Set the current record
        if ($dc) {
            $objRow = $this->Database->prepare("SELECT * FROM tl_tm_cookie WHERE id=?")
                ->limit(1)
                ->execute($intId);

            if ($objRow->numRows) {
                $dc->activeRecord = $objRow;
            }
        }

        $time = time();

        // Update the database
        $this->Database->prepare("UPDATE tl_tm_cookie SET tstamp=$time, published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);

        if ($dc) {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->published = ($blnVisible ? '1' : '');
        }

    }

}
