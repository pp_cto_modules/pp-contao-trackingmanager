<?php

$GLOBALS['TL_DCA']['tl_tm_cookie'] = array
(

    // Config
    'config'      => array
    (
        'dataContainer' =>  \Contao\DC_Table::class,
        'onload_callback' => array
        (
            array(\ppag\TrackingManagerBundle\DataContainer\Cookies::class, 'onloadCallback'),
        ),
        'sql'           => array
        (
            'keys' => array
            (
                'id' => 'primary',
            ),
        ),
    ),

    // List
    'list'        => array
    (
        'sorting'           => array
        (
            'mode'            => 1,
            'fields'          => array('name'),
            'icon'            => 'pagemounts.svg',
            'panelLayout'     => 'filter;search,sort',
            'disableGrouping' => true,
        ),
        'label'             => array
        (
            'fields'      => array('name'),
            'showColumns' => true,
        ),
        'global_operations' => array
        (
            'createBase' => array
            (
                'href'  => 'key=createBase',
                'class' => 'header_theme_import',
            ),
        ),
        'operations'        => array
        (
            'edit'   => array
            (
                'href' => 'act=edit',
                'icon' => 'edit.svg',
            ),
            'delete' => array
            (
                'href'       => 'act=delete',
                'icon'       => 'delete.svg',
                'attributes' => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null ). '\'))return false;Backend.getScrollOffset()"',

            ),
            'toggle' => array
            (
                'icon'            => 'visible.svg',
                'attributes'      => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback' => array(ppag\TrackingManagerBundle\DataContainer\Cookies::class, 'toggleIcon'),
                'showInHeader'    => true,
            ),
            'show'   => array
            (
                'href' => 'act=show',
                'icon' => 'show.svg',
            ),
        ),
    ),

    // Palettes
    'palettes'    => array
    (
        'default' => '{title_legend},published,name,label,descriptions,isBaseCookie,disabled,checked,disabledHint'),

    // Subpalettes
    'subpalettes' => array
    (),

    // Fields
    'fields'      => array
    (
        'id'           => array
        (
            'label' => array('ID'),
            'sql'   => "int(10) unsigned NOT NULL auto_increment",
        ),
        'tstamp'       => array
        (
            'label'   => &$GLOBALS['TL_LANG']['tl_tm_cookie']['tstamp'],
            'sorting' => true,
            'sql'     => "int(10) unsigned NOT NULL default '0'",
        ),
        'pid'          => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_tm_cookie']['pid'],
            'sql'   => "varchar(255) NOT NULL default ''",
        ),
        'name'         => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_tm_cookie']['name'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'inputType' => 'text',
            'eval'      => array('tl_class' => 'w50 clr'),
            'sql'       => "varchar(255) NOT NULL default ''",
        ),
        'label'        => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_tm_cookie']['label'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'inputType' => 'text',
            'eval'      => array('tl_class' => 'w50'),
            'sql'       => "varchar(255) NOT NULL default ''",
        ),
        'isBaseCookie' => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_tm_cookie']['isBaseCookie'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'inputType' => 'checkbox',
            'eval'      => array('submitOnChange' => true, 'tl_class' => 'w50 m12'),
            'sql'       => "varchar(255) NOT NULL default ''",
        ),
        'disabled'    => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_tm_cookie']['disabled'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'inputType' => 'checkbox',
            'eval'      => array('tl_class' => 'w50 m12'),
            'sql'       => "varchar(255) NOT NULL default ''",
        ),
        'checked'    => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_tm_cookie']['checked'],
            'reference' => &$GLOBALS['TL_REF']['tl_tm_cookie']['checked'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'inputType' => 'select',
            'options'   => array('checked','auto'),
            'eval'      => array('tl_class' => 'clr w50','includeBlankOption'=>true),
            'sql'       => "varchar(255) NOT NULL default ''",
        ),
        'published'    => array
        (
            'label'     => &$GLOBALS['TL_LANG']['tl_tm_cookie']['published'],
            'exclude'   => true,
            'search'    => true,
            'sorting'   => true,
            'inputType' => 'checkbox',
            'eval'      => array('tl_class' => 'clr w50'),
            'sql'       => "varchar(255) NOT NULL default ''",
        ),
        'descriptions' => array
        (
            'label'       => &$GLOBALS['TL_LANG']['tl_tm_cookie']['descriptions'],
            'exclude'     => true,
            'inputType'   => 'multiColumnWizard',
            'dragAndDrop' => true,
            'eval'        => [
                // add this line for hide one or all buttons

                'columnFields' => [
                    'label'       => [
                        'label'     => &$GLOBALS['TL_LANG']['tl_tm_cookie']['descriptionLabel'],
                        'exclude'   => true,
                        'inputType' => 'text',
                    ],
                    'description' => [
                        'label'     => &$GLOBALS['TL_LANG']['tl_tm_cookie']['description'],
                        'exclude'   => true,
                        'inputType' => 'text',
                        'eval'      => ['preserveTags' => true, 'allowHtml' => true],
                    ],
                ],
                'tl_class'     => 'm12 clr',
            ],
            'sql'         => 'blob NULL',
        ),
        'templates'    => array
        (
            'inputType'        => 'select',
            'options_callback' => array(
                \ppag\TrackingManagerBundle\DataContainer\Cookies::class,
                'getTemplates',
            ),
            'eval'             => array('multiple' => true, 'chosen' => true),
            'sql'              => "blob NULL",
        ),
        'disabledHint' => array
        (
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'textarea',
            'eval'                    => array('allowHtml'=>true, 'class'=>'monospace', 'rte'=>'ace|html', 'helpwizard'=>true,'tl_class'=>'clr'),
            'explanation'             => 'insertTags',
            'sql'                     => "mediumtext NULL"
        ),
    ),
);
