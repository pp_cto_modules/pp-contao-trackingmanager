<?php

/**
 * Register backend modules
 */

use Contao\System;

$GLOBALS['BE_MOD']['trackingmanager'] = array(

    'tmCookies' => array
    (
        'tables'      => array('tl_tm_cookie'),
        'createBase' => array('ppag\TrackingManagerBundle\Classes\CreateBase', 'createBase'),
    )
);


/**
 * Register models
 */
$GLOBALS['TL_MODELS']['tl_tm_cookie'] = \ppag\TrackingManagerBundle\Model\Cookie::class;


/**
 * Register hooks
 */
$GLOBALS['TL_HOOKS']['generatePage'][] = array(\ppag\TrackingManagerBundle\Hook\GeneratePage::class, 'generateTrackingManager');
$GLOBALS['TL_HOOKS']['parseFrontendTemplate'][] = array(\ppag\TrackingManagerBundle\Hook\ParseFrontendTemplate::class, 'checkCookieDependency');
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array(\ppag\TrackingManagerBundle\Hook\ReplaceInsertTags::class, 'replaceTrackingManagerEditor');


/**
 * allowed pagetypes config
 */
$GLOBALS['TM_PAGETYPES'] = ['regular'];