<?php

namespace ppag\TrackingManagerBundle\Hook;

use Contao\LayoutModel;
use Contao\PageModel;
use Contao\PageRegular;
use ppag\TrackingManagerBundle\Classes\TrackingManager;

class GeneratePage
{
    /**
     *
     *
     * @param PageModel   $page
     * @param LayoutModel $layout
     * @param PageRegular $pageRegular
     */
    public function generateTrackingManager(PageModel $page, LayoutModel $layout, PageRegular $pageRegular)
    {
        $trackingManager = new TrackingManager();
        $trackingManager->setPage($page);
        $trackingManager->generate();
    }
}
