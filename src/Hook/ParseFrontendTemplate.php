<?php

namespace ppag\TrackingManagerBundle\Hook;

use Contao\FrontendTemplate;
use Contao\StringUtil;
use Contao\System;
use ppag\TrackingManagerBundle\Classes\TrackingManagerStatus;
use ppag\TrackingManagerBundle\Model\Cookie;
use Symfony\Component\VarDumper\VarDumper;

class ParseFrontendTemplate
{

    public function checkCookieDependency(string $buffer, string $template)
    {


        $request = System::getContainer()->get('request_stack')->getCurrentRequest();
        if ($request && System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest($request)) {
            return $buffer;
        }

        $cookies = Cookie::getCookiesByRootpage();
        if ($cookies === null) {
            return $buffer;
        }

        $cancelOutput = false;

        foreach ($cookies as $cookie) {
            if ($cookie->isBaseCookie) {
                continue;
            }

            $templates = StringUtil::deserialize($cookie->templates, true);
            if (!in_array($template, $templates)) {
                continue;
            }

            if (!TrackingManagerStatus::getCookieStatus($cookie->name)) {
                $noCookiehint = '';
                if ($cookie->disabledHint) {
                    $tpl = new FrontendTemplate('trackingmanager_declined-cookie');
                    $tpl->hint = $cookie->disabledHint;
                    $noCookiehint = $tpl->parse();
                }

                $cancelOutput = true;
            } else {
                break;
            }
        }

        if ($cancelOutput) {
            return $noCookiehint;
        }

        return $buffer;
    }

}
