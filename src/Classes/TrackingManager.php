<?php

namespace ppag\TrackingManagerBundle\Classes;

use Contao\Combiner;
use Contao\Controller;
use Contao\CoreBundle\Monolog\ContaoContext;
use Contao\CoreBundle\Session\Attribute\ArrayAttributeBag;
use Contao\Environment;
use Contao\FormCheckbox;
use Contao\FrontendTemplate;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\System;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use ppag\TrackingManagerBundle\Model\Cookie;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TrackingManager
{

    /** @var PageModel */
    protected $page;

    /** @var SessionInterface */
    protected $session;

    /** @var ArrayAttributeBag */
    protected $frontendSession;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * @return Session
     */
    public function getSession(): Session
    {
        return $this->session;
    }

    /**
     * @param PageModel $page
     */
    public function setPage(PageModel $page): void
    {
        $this->page = $page;
    }

    /**
     * @return PageModel
     */
    public function getPage(): PageModel
    {
        return $this->page;
    }

    /**
     *
     */
    public function __construct()
    {
        $this->logger = System::getContainer()->get('monolog.logger.contao');
        $this->requestStack = System::getContainer()->get('request_stack');
        $this->request = $this->requestStack->getCurrentRequest();

        // check sessionbag to save saved config
        $this->session =$this->request->getSession();
        $this->frontendSession = $this->session->getBag('contao_frontend');
    }


    public function generate()
    {
        if (!in_array($this->page->type, $GLOBALS['TM_PAGETYPES'])) {
            return;
        }

        $rootPage = PageModel::findById($this->page->rootId);
        if (!$rootPage->tm_active) {
            return;
        }

        // No cookies selected
        $cookies = StringUtil::deserialize($rootPage->tm_cookies, true);
        if (empty($cookies)) {
            return;
        }

        $cookieSettings = Cookie::getCookiesByRootpage($rootPage);
        $baseCookie = Cookie::getBaseCookieByRootPage($rootPage);

        // get cookies set vs available values
        if ($cookieSettings === null) {
            $this->logger->log(LogLevel::INFO, 'No cookies selected in root page.', array('contao' => new ContaoContext(__METHOD__, 'Tracking Manager')));
            return;
        }
        if ($baseCookie === null) {
            $this->logger->log(LogLevel::INFO, 'No baseCookie selected in root page.', array('contao' => new ContaoContext(__METHOD__, 'Tracking Manager')));
            return;
        }

        $arrCookies = $this->getCookieData();
        $this->frontendSession->remove('tm_config_set');

        // template and frontend logic
        $config = $this->getConfiguration();
        $savedConfig = TrackingManagerStatus::getBaseCookieValue();

        foreach ($arrCookies as $name => $cookie) {
            $arrCookies[$name]['widget'] = $this->parseWidgetForCookie($cookie);
        }

        $template = new FrontendTemplate('trackingmanager');
        $template->headline = $rootPage->tm_headline;
        $template->intro = $rootPage->tm_intro;

        if ($rootPage->tm_link) {
            $objPage = PageModel::findByPk($rootPage->tm_link);
            $linkUrl = $objPage->getFrontendUrl();
            $linkText = $rootPage->tm_linktext;

            if ($linkText === '') {
                $linkText = $linkUrl;
            }

            $link = sprintf('<a href="%s" target="_blank">%s</a>', $linkUrl, $linkText);
            $template->intro = str_replace('{{link}}', $link, $template->intro);

            //set TM hidden if on legal pages
            if ($this->page->id == $objPage->id) {
                $template->hidden = true;
            }

        }

        $template->baseCookieName = TrackingManagerStatus::getBaseCookieName();
        $template->linktext = $rootPage->tm_linktext;
        $template->submit_all = $rootPage->tm_submit_all;
        $template->deny_all = $rootPage->tm_deny_all;
        $template->details = ($rootPage->tm_dynamic) ? '' : $rootPage->tm_details;
        $template->submit = $rootPage->tm_submit;
        $template->cookies = $arrCookies;
        $template->isDynamic = $rootPage->tm_dynamic;
        $template->asOverlay = $rootPage->tm_overlay;
        $template->config = sha1(serialize($arrCookies));

        if (TrackingManagerStatus::isBaseCookieSet() && ($config == $savedConfig)) {
            $template->hidden = true;
        }

        $GLOBALS['TL_BODY'][] = System::getContainer()->get('contao.insert_tag.parser')->replaceInline($template->parse()); // see #4268
        $combiner = new Combiner();
        $combiner->add('bundles/trackingmanager/css/trackingmanager.scss');
        $GLOBALS['TL_BODY'][] = '<link rel="stylesheet" href="'.$combiner->getCombinedFile().'">';

        // add js
        $jsTemplate = new FrontendTemplate('trackingmanagerjs');
        $jsTemplate->cookiesTTL = $rootPage->tm_cookies_ttl;
        $jsTemplate->isDynamic = ($rootPage->tm_dynamic) ? '1' : 'false';
        $jsTemplate->dynamicScripts = ($rootPage->tm_dynamic_scripts) ? $rootPage->tm_dynamic_scripts : '';
        $GLOBALS['TL_BODY'][] = $jsTemplate->parse();

        // save config preparation
        $this->frontendSession->set('tm_config_set', 1);

        // Include default trackingmanager editor.
        if ($rootPage->tm_editable) {
            $editorTemplate = new FrontendTemplate('trackingmanager_editor');

            // Hide default trackingmanager editor if the trackingmanager is shown anyway.
            if (!TrackingManagerStatus::isBaseCookieSet() || ($config != $savedConfig)) {
                $editorTemplate->hidden = true;
            }

            //show Editorbutton if TM is hidden
            if ($template->hidden) {
                $editorTemplate->hidden = false;
            }

            $GLOBALS['TL_BODY'][] = System::getContainer()->get('contao.insert_tag.parser')->replaceInline($editorTemplate->parse()); // see #4268
        }

        $this->deleteDisabledBrowserCookies();
    }

    protected function getCookieData()
    {
        $rootPage = PageModel::findById($this->page->rootId);
        $cookieSettings = Cookie::getCookiesByRootpage($rootPage);

        /** @var Cookie $cookieSettings */
        while ($cookieSettings->next()) {

            $cookie = $cookieSettings->row();

            $descriptions = StringUtil::deserialize($cookie['descriptions'], true);
            $firstDescription = $descriptions[0];
            $keys = array_keys($firstDescription);
            $firstKey = $keys[0];

            if (!empty($firstDescription[$firstKey])) {
                $cookie['descriptions'] = $descriptions;
            } else {
                $cookie['descriptions'] = array();
            }

            $arrCookies[$cookieSettings->name] = $cookie;
        }

        return $arrCookies;
    }

    /**
     * @param $cookie
     * @return bool
     */
    public function isBaseCookie($cookie)
    {
        if ($cookie['name'] === TrackingManagerStatus::getBaseCookieName()) {
            return true;
        }

        return false;
    }

    protected function parseWidgetForCookie($cookie)
    {
        // Generate checkbox widget
        $widget = new FormCheckBox();
        $widget->name = $cookie['name'];
        $widget->id = 'tm_' . $cookie['id'];

        if ($cookie['disabled'] or $this->isBaseCookie($cookie)) {
            $widget->disabled = true;
        }

        $options = array
        (
            'value' => ($cookie['name'] === TrackingManagerStatus::getBaseCookieName()) ? $this->getConfiguration() : '1',
            'label' => $cookie['label'],
        );

        // this sets the "checked" attribute
        if ($cookie['checked'] === 'checked' or $this->isBaseCookie($cookie)) {
            $options['default'] = true;
        } elseif ($cookie['checked'] === 'auto') {
            $options['default'] = TrackingManagerStatus::getCookieStatus($cookie['name']);
        }

        $widget->options = array($options);

        return $widget->parse();
    }

    protected function getConfiguration()
    {
        return sha1(serialize($this->getCookieData()));
    }

    protected function deleteDisabledBrowserCookies()
    {
        $cookies = $this->getCookieData();

        $domain = Environment::get('host');

        // Get subdomain
        $firstDot = strpos($domain, '.');
        $subdomain = substr($domain, 0, $firstDot);

        $domains = array
        (
            $domain,
            '.' . $domain,
            str_replace($subdomain . '.', '', $domain),
            '.' . str_replace($subdomain . '.', '', $domain),
        );

        foreach ($cookies as $cookie) {
            $cookieModel = Cookie::findByPk($cookie['id']);

            if (TrackingManagerStatus::getCookieStatus($cookieModel->name) === true) {
                continue;
            }

            $browserCookies = $cookieModel->getBrowserCookieNames();
            foreach ($browserCookies as $name) {
                foreach ($domains as $d) {
                    System::setCookie($name, '', time() - 3600, '/', $d);
                }
            }
        }
    }

}
