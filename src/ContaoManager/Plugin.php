<?php

/*
 * This file is part of Contao Simple SVG Icons Bundle.
 *
 * (c) pp
 *
 * @license LGPL-3.0-or-later
 */

namespace ppag\TrackingManagerBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use ppag\TrackingManagerBundle\TrackingManagerBundle;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(TrackingManagerBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
