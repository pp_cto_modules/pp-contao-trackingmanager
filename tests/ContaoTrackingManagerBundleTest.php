<?php

/*
 * This file is part of Contao Simple SVG Icons Bundle.
 *
 * (c) slashworks
 *
 * @license LGPL-3.0-or-later
 */

namespace PP\TrackingManagerBundle\Tests;

use PHPUnit\Framework\TestCase;
use PP\TrackingManagerBundle\TrackingManagerBundle;

class ContaoTrackingManagerBundleTest extends TestCase
{
    public function testCanBeInstantiated()
    {
        $bundle = new TrackingManagerBundle();
        $this->assertInstanceOf('PP\ContaoTrackingManagerBundle\ContaoTrackingManagerBundleTest', $bundle);
    }
}
